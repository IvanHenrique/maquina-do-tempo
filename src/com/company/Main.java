package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner ler = new Scanner(System.in);
        System.out.println("Teste de lógica! \n Maquina do Tempo");

        System.out.println("Entrada:");

        int dia = ler.nextInt();
        int mes = ler.nextInt();
        int ano = ler.nextInt();

        System.out.println("Entrada: " + dia + "/" + mes + "/" + ano);

        System.out.println("Saida: " + MaquinaDoTempo(dia, mes, ano));

    }
    private static boolean MaquinaDoTempo(int dia, int mes, int ano){

        if (isDataValida(dia, mes, ano)) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean isDataValida(int dia, int mes, int ano) {

        return (isAnoValido(ano) && isMesValido(mes) && isDiaValido(dia, mes, ano));
    }

    private static boolean isAnoValido(int ano) {
        return (ano >= 1);
    }

    private static boolean isAnoBissexto(int ano) {
        // Divisivel por 4. Sendo assim, a divisao e exata com o resto igual a
        // zero;
        // Nao pode ser divisivel por 100. Com isso, a divisao nao e exata, ou
        // seja, deixa resto diferente de zero;
        // Pode ser que seja divisivel por 400. Caso seja divisivel por 400, a
        // divisao deve ser exata, deixando o resto igual a zero.

        return ((ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0)));
    }

    private static boolean isMesValido(int mes) {
        return (mes >= 1 && mes <= 12);
    }

    private static int getQuantidadeDiasNoMes(int mes, int ano) {
        switch (mes) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            case 2:
                if (isAnoBissexto(ano)) {
                    return 29;
                }
                return 28;
        }

        return -1;
    }

    private static boolean isDiaValido(int dia, int mes, int ano) {
        int qtDias = getQuantidadeDiasNoMes(mes, ano);
        return (dia > 0 && dia <= qtDias);
    }

}
